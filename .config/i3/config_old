#### Joris' i3 config file

#  _______  _______  ______  _________ _______ _________ _______  _______  _______
# (       )(  ___  )(  __  \ \__   __/(  ____ \\__   __/(  ____ \(  ____ )(  ____ \
# | () () || (   ) || (  \  )   ) (   | (    \/   ) (   | (    \/| (    )|| (    \/
# | || || || |   | || |   ) |   | |   | (__       | |   | (__    | (____)|| (_____
# | |(_)| || |   | || |   | |   | |   |  __)      | |   |  __)   |     __)(_____  )
# | |   | || |   | || |   ) |   | |   | (         | |   | (      | (\ (         ) |
# | )   ( || (___) || (__/  )___) (___| )      ___) (___| (____/\| ) \ \__/\____) |
# |/     \|(_______)(______/ \_______/|/       \_______/(_______/|/   \__/\_______)

# Set modifier key
set $mod Mod4

# Direction keys
set $up l
set $down k
set $left j
set $right semicolon

# Workspace naming
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"

#  _        _______  _______  _
# ( \      (  ___  )(  ___  )| \    /\
# | (      | (   ) || (   ) ||  \  / /
# | |      | |   | || |   | ||  (_/ /
# | |      | |   | || |   | ||   _ (
# | |      | |   | || |   | ||  ( \ \
# | (____/\| (___) || (___) ||  /  \ \
# (_______/(_______)(_______)|_/    \/

# thin borders
hide_edge_borders both

# Workspace layouts
workspace_layout default

# Border & Title
for_window [class="^.*"] border pixel 1, title_format "<b> %class >> %title </b>"

# Font
font pango:Droid Sans Regular 11

#  ______   _______  _______  ______   _______  _______      __      _______  _______  _______  _______
# (  ___ \ (  ___  )(  ____ )(  __  \ (  ____ \(  ____ )    /__\    (  ____ \(  ___  )(  ____ )(  ____ \
# | (   ) )| (   ) || (    )|| (  \  )| (    \/| (    )|   ( \/ )   | (    \/| (   ) || (    )|| (    \/
# | (__/ / | |   | || (____)|| |   ) || (__    | (____)|    \  /    | |      | (___) || (____)|| (_____
# |  __ (  | |   | ||     __)| |   | ||  __)   |     __)    /  \/\  | | ____ |  ___  ||  _____)(_____  )
# | (  \ \ | |   | || (\ (   | |   ) || (      | (\ (      / /\  /  | | \_  )| (   ) || (            ) |
# | )___) )| (___) || ) \ \__| (__/  )| (____/\| ) \ \__  (  \/  \  | (___) || )   ( || )      /\____) |
# |/ \___/ (_______)|/   \__/(______/ (_______/|/   \__/   \___/\/  (_______)|/     \||/       \_______)

# Gaps
gaps inner 8
gaps outer 0

# Enable the following in case of active i3bar.
#gaps top -8


new_window normal
new_float  normal

hide_edge_borders both

bindsym $Mod+shift+b border toggle

# Changing border style
bindsym $Mod+n border normal
bindsym $Mod+y border 1pixel
bindsym $Mod+u border none

# Change gaps
bindsym $Mod+plus                gaps inner current plus  5
bindsym $Mod+minus               gaps inner current minus 5
bindsym $Mod+Shift+plus          gaps outer current plus  5
bindsym $Mod+Shift+minus         gaps outer current minus 5
bindsym $Mod+Control+plus        gaps inner all     plus  5
bindsym $Mod+Control+minus       gaps inner all     minus 5
bindsym $Mod+Control+Shift+plus  gaps outer all     plus  5
bindsym $Mod+Control+Shift+minus gaps outer all     minus 5

smart_gaps on

#  _______  _______  _        _______  _______  _______
# (  ____ \(  ___  )( \      (  ___  )(  ____ )(  ____ \
# | (    \/| (   ) || (      | (   ) || (    )|| (    \/
# | |      | |   | || |      | |   | || (____)|| (_____
# | |      | |   | || |      | |   | ||     __)(_____  )
# | |      | |   | || |      | |   | || (\ (         ) |
# | (____/\| (___) || (____/\| (___) || ) \ \__/\____) |
# (_______/(_______)(_______/(_______)|/   \__/\_______)

# Windows
set $bg-color            #2f343f
set $inactive-bg-color   #2f343f
set $text-color          #f3f4f5
set $inactive-text-color #676e7d
set $urgent-bg-color     #e53935
set $indicator-color     #a0a0a0

# Bar

# 1: border
# 2: background
# 3: text

set $separator           #666666
set $background          #00000000
set $statusline          #dddddd
set $focused_workspace1  #00000000
set $focused_workspace2  #00000000
set $focused_workspace3  #a47db4
set $active_workspace1   #00000000
set $active_workspace2   #00000000
set $active_workspace3   #ffffff
set $inactive_workspace1 #00000000
set $inactive_workspace2 #00000000
set $inactive_workspace3 #ffffff
set $urgent_workspace1   #00000000
set $urgent_workspace2   #900000
set $urgent_workspace3   #ffffff

# Set Window color
#                       border             background         text                 indicator
client.focused          $bg-color          $bg-color          $text-color          $indicator-color
client.unfocused        $inactive-bg-color $inactive-bg-color $inactive-text-color $indicator-color
client.focused_inactive $inactive-bg-color $inactive-bg-color $inactive-text-color $indicator-color
client.urgent           $urgent-bg-color   $urgent-bg-color   $text-color          $indicator-color



#  ______   _______  _______
# (  ___ \ (  ___  )(  ____ )
# | (   ) )| (   ) || (    )|
# | (__/ / | (___) || (____)|
# |  __ (  |  ___  ||     __)
# | (  \ \ | (   ) || (\ (
# | )___) )| )   ( || ) \ \__
# |/ \___/ |/     \||/   \__/

bar {
      i3bar_command i3bar -t
      status_command i3blocks -c ~/.config/i3/i3blocks.conf
      position top
      mode hide
      hidden_state hide
      modifier none
      tray_padding 0
      colors {
          separator          $separator
          background         $background
          statusline         $statusline
          focused_workspace  $focused_workspace1 $focused_workspace2 $focused_workspace3
          active_workspace   $active_workspace1 $active_workspace2 $active_workspace3
          inactive_workspace $inactive_workspace1 $inactive_workspace2 $inactive_workspace3
          urgent_workspace   $urgent_workspace1 $urgent_workspace2 $urgent_workspace3
    }
}

#  _______          _________ _______  _______ _________ _______  _______ _________
# (  ___  )|\     /|\__   __/(  ___  )(  ____ \\__   __/(  ___  )(  ____ )\__   __/
# | (   ) || )   ( |   ) (   | (   ) || (    \/   ) (   | (   ) || (    )|   ) (
# | (___) || |   | |   | |   | |   | || (_____    | |   | (___) || (____)|   | |
# |  ___  || |   | |   | |   | |   | |(_____  )   | |   |  ___  ||     __)   | |
# | (   ) || |   | |   | |   | |   | |      ) |   | |   | (   ) || (\ (      | |
# | )   ( || (___) |   | |   | (___) |/\____) |   | |   | )   ( || ) \ \__   | |
# |/     \|(_______)   )_(   (_______)\_______)   )_(   |/     \||/   \__/   )_(

# Networkmanager-applet
exec --no-startup-id nm-applet

# Wallpaper
exec --no-startup-id nitrogen --restore

# Powersavings for display:
exec --no-startup-id xset s 480 dpms 600 600 600

# Desktop notifications
exec --no-startup-id /usr/bin/dunst

# Update notification should be set to tray and notification
#exec --no-startup-id sleep 15 && eos-update-notifier
#exec --no-startup-id sleep 15 && eos-welcome

# Get auth work with polkit-gnome
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1

# Autotiling
exec_always --no-startup-id autotiling

# Powermanager
exec --no-startup-id xfce4-power-manager

# Clipboard
exec --no-startup-id clipit

# Capslock to escape
exec --no-startup-id xmodmap -e "clear lock" #disable caps lock switch
exec --no-startup-id xmodmap -e "keysym Caps_Lock = Escape" #set caps_lock as escape

# Hide the mouse pointer if unused for a duration
exec --no-startup-id /usr/bin/unclutter -b

# Compton / Picom
exec_always --no-startup-id picom --config ~/.config/picom/picom.conf

# Disable touchpad when mouse is plugged in
exec --no-startup-id touchpad-indicator

#  ______  _________ _        ______  _________ _        _______  _______
# (  ___ \ \__   __/( (    /|(  __  \ \__   __/( (    /|(  ____ \(  ____ \
# | (   ) )   ) (   |  \  ( || (  \  )   ) (   |  \  ( || (    \/| (    \/
# | (__/ /    | |   |   \ | || |   ) |   | |   |   \ | || |      | (_____
# |  __ (     | |   | (\ \) || |   | |   | |   | (\ \) || | ____ (_____  )
# | (  \ \    | |   | | \   || |   ) |   | |   | | \   || | \_  )      ) |
# | )___) )___) (___| )  \  || (__/  )___) (___| )  \  || (___) |/\____) |
# |/ \___/ \_______/|/    )_)(______/ \_______/|/    )_)(_______)\_______)

# Reload the configuration file
bindsym $mod+Shift+c reload

# Restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# Kill focused window
bindsym $mod+x kill

# Launch config
bindsym $mod+c exec --no-startup-id leafpad ~/.config/i3/config

# Toggle i3bar
bindsym $mod+Escape exec i3-msg bar mode toggle

# Start a terminal
bindsym $mod+Return exec --no-startup-id  xfce4-terminal
#bindsym $mod+Return exec --no-startup-id  termite

# Exit i3 (logs you out of your X session)
#bindsym $mod+Shift+Escape exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# Dmenu
bindsym $mod+d exec --no-startup-id i3-dmenu-desktop

# App shortcuts
bindsym $mod+w exec "/usr/bin/firefox"
bindsym $mod+t exec "/usr/bin/nemo"

# Volume
#bindsym XF86AudioRaiseVolume exec amixer -D pulse set Master 5%+ && pkill -RTMIN+1 i3blocks
#bindsym XF86AudioLowerVolume exec amixer -D pulse set Master 5%- && pkill -RTMIN+1 i3blocks

# Volume with Dunst notification
bindsym XF86AudioRaiseVolume exec --no-startup-id /home/joris/github/dotfiles/scripts/volumeControl.sh up
bindsym XF86AudioLowerVolume exec --no-startup-id /home/joris/github/dotfiles/scripts/volumeControl.sh down

# Granular volume control
bindsym $mod+XF86AudioRaiseVolume exec amixer -D pulse set Master 1%+ && pkill -RTMIN+1 i3blocks
bindsym $mod+XF86AudioLowerVolume exec amixer -D pulse set Master 1%- && pkill -RTMIN+1 i3blocks

# Mute
#bindsym XF86AudioMute exec amixer set Master toggle && killall -USR1 i3blocks # original EndeavourOS binding
#bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle # actually toggling binding

# Mute with Dunst notification
bindsym XF86AudioMute exec --no-startup-id /home/joris/github/dotfiles/scripts/volumeControl.sh mute

# Backlight
#bindsym XF86MonBrightnessUp   exec --no-startup-id xbacklight -inc 10
#bindsym XF86MonBrightnessDown exec --no-startup-id xbacklight -dec 10

# Backlight with Dunst notification
bindsym XF86MonBrightnessUp   exec --no-startup-id /home/joris/github/dotfiles/scripts/brightnessControl.sh up
bindsym XF86MonBrightnessDown exec --no-startup-id /home/joris/github/dotfiles/scripts/brightnessControl.sh down

# Multimedia playback
#bindsym XF86AudioPlay exec playerctl play
#bindsym XF86AudioPause exec playerctl pause
#bindsym XF86AudioNext exec playerctl next
#bindsym XF86AudioPrev exec playerctl previous

bindsym XF86AudioPlay exec mocp --toggle-pause
bindsym XF86AudioPause exec mocp --toggle-pause
bindsym XF86AudioStop exec mocp --togle-pause
bindsym XF86AudioNext exec mocp --next
bindsym XF86AudioPrev exec mocp --previous

bindsym $mod+F5 exec mocp --toggle-pause
bindsym $mod+F6 exec mocp --stop
bindsym $mod+F7 exec mocp --previous
bindsym $mod+F8 exec mocp --next

# Screenshot
bindsym Print exec "scrot ~/Pictures/%Y-%m-%d-%T-screenshot.png"

# Redirect sound to headphones
bindsym $mod+m exec "/usr/local/bin/switch-audio-port"

# Rofi bindings simple menu
#bindsym F9 exec rofi -modi drun -show drun -lines 7
#bindsym F10 exec rofi -show run   -lines 7
#bindsym F12 exec rofi -show window -lines 7

# Lock the system
bindsym Mod1+Escape exec i3lock -i ~/.config/i3/i3-lock-screen.png -t -f

# Log out
bindsym $mod+End exec --no-startup-id i3exit logout, mode "default"

# Shut down
bindsym $mod+Delete exec --no-startup-id i3exit shutdown, mode "default"

# Rofi bindings fancy menu
bindsym $mod+space exec rofi -modi drun -show drun -line-padding 4 \
                -columns 2 -padding 50 -hide-scrollbar \
                -show-icons -drun-icon-theme "Arc-X-D" -font "Droid Sans Regular 10"

bindsym F9 exec rofi -modi drun -show drun -line-padding 4 \
                -columns 2 -padding 50 -hide-scrollbar \
                -show-icons -drun-icon-theme "Arc-X-D" -font "Droid Sans Regular 10"

bindsym Mod1+o exec rofi -show window -line-padding 4 \
                -lines 6 -padding 50 -hide-scrollbar \
                -show-icons -drun-icon-theme "Arc-X-D" -font "Droid Sans Regular 10"

bindsym F10 exec rofi -show window -line-padding 4 \
                -lines 6 -padding 50 -hide-scrollbar \
                -show-icons -drun-icon-theme "Arc-X-D" -font "Droid Sans Regular 10"

# Set shut down, restart and locking features
#bindsym $mod+Escape mode "$mode_system"
#set $mode_system (l)ock, (e)xit, switch_(u)ser, (s)uspend, (h)ibernate, (r)eboot, (Shift+s)hutdown
#mode "$mode_system" {
#    bindsym l exec --no-startup-id i3exit lock, mode "default"
#    bindsym s exec --no-startup-id i3exit suspend, mode "default"
#    bindsym u exec --no-startup-id i3exit switch_user, mode "default"
#    bindsym e exec --no-startup-id i3exit logout, mode "default"
#    bindsym h exec --no-startup-id i3exit hibernate, mode "default"
#    bindsym r exec --no-startup-id i3exit reboot, mode "default"
#    bindsym Shift+s exec --no-startup-id i3exit shutdown, mode "default"

    # exit system mode: "Enter" or "Escape"
#    bindsym Return mode "default"
#    bindsym Escape mode "default"
#}

# Switching GPU
bindsym $mod+shift+i exec --no-startup-id optimus-manager --switch intel --no-confirm
bindsym $mod+shift+n exec --no-startup-id optimus-manager --switch nvidia --no-confirm

# Launch Atom
bindsym $mod+a exec --no-startup-id atom

#            _______  _______  _        _______  _______  _______  _______  _______    ______  _________ _        ______  _________ _        _______  _______
# |\     /|(  ___  )(  ____ )| \    /\(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \  (  ___ \ \__   __/( (    /|(  __  \ \__   __/( (    /|(  ____ \(  ____ \
# | )   ( || (   ) || (    )||  \  / /| (    \/| (    )|| (   ) || (    \/| (    \/  | (   ) )   ) (   |  \  ( || (  \  )   ) (   |  \  ( || (    \/| (    \/
# | | _ | || |   | || (____)||  (_/ / | (_____ | (____)|| (___) || |      | (__      | (__/ /    | |   |   \ | || |   ) |   | |   |   \ | || |      | (_____
# | |( )| || |   | ||     __)|   _ (  (_____  )|  _____)|  ___  || |      |  __)     |  __ (     | |   | (\ \) || |   | |   | |   | (\ \) || | ____ (_____  )
# | || || || |   | || (\ (   |  ( \ \       ) || (      | (   ) || |      | (        | (  \ \    | |   | | \   || |   ) |   | |   | | \   || | \_  )      ) |
# | () () || (___) || ) \ \__|  /  \ \/\____) || )      | )   ( || (____/\| (____/\  | )___) )___) (___| )  \  || (__/  )___) (___| )  \  || (___) |/\____) |
# (_______)(_______)|/   \__/|_/    \/\_______)|/       |/     \|(_______/(_______/  |/ \___/ \_______/|/    )_)(______/ \_______/|/    )_)(_______)\_______)

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6

# switch to next or previous workspace
bindsym $mod+Mod1+Left workspace prev
bindsym $mod+Mod1+Right workspace next

#          _________ _        ______   _______             _______  _______  _       _________ _______  _______  _
# |\     /|\__   __/( (    /|(  __  \ (  ___  )|\     /|  (  ____ \(  ___  )( (    /|\__   __/(  ____ )(  ___  )( \
# | )   ( |   ) (   |  \  ( || (  \  )| (   ) || )   ( |  | (    \/| (   ) ||  \  ( |   ) (   | (    )|| (   ) || (
# | | _ | |   | |   |   \ | || |   ) || |   | || | _ | |  | |      | |   | ||   \ | |   | |   | (____)|| |   | || |
# | |( )| |   | |   | (\ \) || |   | || |   | || |( )| |  | |      | |   | || (\ \) |   | |   |     __)| |   | || |
# | || || |   | |   | | \   || |   ) || |   | || || || |  | |      | |   | || | \   |   | |   | (\ (   | |   | || |
# | () () |___) (___| )  \  || (__/  )| (___) || () () |  | (____/\| (___) || )  \  |   | |   | ) \ \__| (___) || (____/\
# (_______)\_______/|/    )_)(______/ (_______)(_______)  (_______/(_______)|/    )_)   )_(   |/   \__/(_______)(_______/

# Scratchpad, Floating
#bindsym $Mod+space floating toggle
floating_modifier  Mod1

bindsym $Mod+Shift+z move scratchpad
bindsym $Mod+z scratchpad show

# Change focus
bindsym $Mod+$left  focus left
bindsym $Mod+$down  focus down
bindsym $Mod+$up    focus up
bindsym $Mod+$right focus right

# Alternatively, you can use the cursor keys:
bindsym $Mod+Left  focus left
bindsym $Mod+Down  focus down
bindsym $Mod+Up    focus up
bindsym $Mod+Right focus right

#bindsym $Mod+p focus parent
#bindsym $Mod+c focus child

# Move focused window
bindsym $Mod+Shift+$left  move left  10px
bindsym $Mod+Shift+$down  move down  10px
bindsym $Mod+Shift+$up    move up    10px
bindsym $Mod+Shift+$right move right 10px

# Alternatively, you can use the cursor keys:
bindsym $Mod+Shift+Up    move up    10px
bindsym $Mod+Shift+Down  move down  10px
bindsym $Mod+Shift+Left  move left  10px
bindsym $Mod+Shift+Right move right 10px

# Size
bindsym Mod1+Up    resize shrink height 10 px or 1 ppt
bindsym Mod1+Down  resize grow   height 10 px or 1 ppt
bindsym Mod1+Left  resize shrink width  10 px or 1 ppt
bindsym Mod1+Right resize grow   width  10 px or 1 ppt

# Resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym ntilde resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Layout toggle, keycode 23 is Tab
#bindcode Mod1+23 layout toggle tabbed split
#bindcode $Mod+23 layout toggle splitv splith

# Switch to workspace with urgent window
for_window [urgent="latest"] focus
focus_on_window_activation   focus

# Container layout
bindsym $Mod+h split h
bindsym $Mod+v split v
bindsym $Mod+Shift+t layout tabbed
bindsym $Mod+Shift+s layout stacking
bindsym $Mod+Shift+h layout toggle split

# Toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# Enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

default_orientation horizontal

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# Focus the parent container (never use it, rather have it for Atom.)
# bindsym $mod+a focus parent

# Focus the child container
#bindsym $mod+d focus child

# Resize floating windows with mouse scroll:
bindsym --whole-window --border $mod+button4 resize shrink height 5 px or 5 ppt
bindsym --whole-window --border $mod+button5 resize grow height 5 px or 5 ppt
bindsym --whole-window --border $mod+shift+button4 resize shrink width 5 px or 5 ppt
bindsym --whole-window --border $mod+shift+button5 resize grow width 5 px or 5 ppt

#  _______  _______  _______  _       _________ _______  _______ __________________ _______  _          _______  _______ ___________________________ _        _______  _______
# (  ___  )(  ____ )(  ____ )( \      \__   __/(  ____ \(  ___  )\__   __/\__   __/(  ___  )( (    /|  (  ____ \(  ____ \\__   __/\__   __/\__   __/( (    /|(  ____ \(  ____ \
# | (   ) || (    )|| (    )|| (         ) (   | (    \/| (   ) |   ) (      ) (   | (   ) ||  \  ( |  | (    \/| (    \/   ) (      ) (      ) (   |  \  ( || (    \/| (    \/
# | (___) || (____)|| (____)|| |         | |   | |      | (___) |   | |      | |   | |   | ||   \ | |  | (_____ | (__       | |      | |      | |   |   \ | || |      | (_____
# |  ___  ||  _____)|  _____)| |         | |   | |      |  ___  |   | |      | |   | |   | || (\ \) |  (_____  )|  __)      | |      | |      | |   | (\ \) || | ____ (_____  )
# | (   ) || (      | (      | |         | |   | |      | (   ) |   | |      | |   | |   | || | \   |        ) || (         | |      | |      | |   | | \   || | \_  )      ) |
# | )   ( || )      | )      | (____/\___) (___| (____/\| )   ( |   | |   ___) (___| (___) || )  \  |  /\____) || (____/\   | |      | |   ___) (___| )  \  || (___) |/\____) |
# |/     \||/       |/       (_______/\_______/(_______/|/     \|   )_(   \_______/(_______)|/    )_)  \_______)(_______/   )_(      )_(   \_______/|/    )_)(_______)\_______)

# Set floating for apps needing it
for_window [class="Pavucontrol" instance="pavucontrol"] floating enable
for_window [class="Yad" instance="yad"] floating enable
for_window [class="Galculator" instance="galculator"] floating enable
for_window [class="Blueberry.py" instance="blueberry.py"] floating enable
#for_window [class="Leafpad" instance="leafpad"] floating enable
#for_window [class="Leafpad"] resize set 900 700

# Set floating (nontiling) for special apps
for_window [class="Xsane" instance="xsane"] floating enable
for_window [class="Pavucontrol" instance="pavucontrol"] floating enable
for_window [class="qt5ct" instance="qt5ct"] floating enable
for_window [class="Blueberry.py" instance="blueberry.py"] floating enable
for_window [class="Bluetooth-sendto" instance="bluetooth-sendto"] floating enable
for_window [class="Yad" instance="yad"] floating enable
for_window [class="Pamac-manager"] floating enable

popup_during_fullscreen smart
