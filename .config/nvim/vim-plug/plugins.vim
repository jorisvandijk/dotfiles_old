" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

    " Better Syntax Support
    Plug 'sheerun/vim-polyglot'
    " Auto pairs for '(' '[' '{'
    Plug 'jiangmiao/auto-pairs'
    " Autocompletion 
    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
    " Python support
    Plug 'zchee/deoplete-jedi'
    " Airline status bar
    Plug 'vim-airline/vim-airline'
    " vifm file maneger
    Plug 'vifm/vifm.vim'
    " Nord theme
    Plug 'arcticicestudio/nord-vim'
    " i3 highlighting
    Plug 'mboughaba/i3config.vim'

    call plug#end()
