set expandtab
set number
set hidden
set ignorecase
set shiftwidth=4
set smartcase
set mouse=a
set nobackup
set tabstop=4      
source $HOME/.config/nvim/vim-plug/plugins.vim
let g:deoplete#enable_at_startup = 1
colorscheme nord
aug i3config_ft_detection
  au!
  au BufNewFile,BufRead ~/.config/i3/config set filetype=i3config
aug end
